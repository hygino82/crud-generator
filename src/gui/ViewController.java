package gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class ViewController {

	@FXML
	private TextField txtBasePackage;

	@FXML
	private TextField txtEntityName;

	@FXML
	private TextField txtModel;

	@FXML
	private TextField txtService;

	@FXML
	private TextField txtController;

	@FXML
	private TextField txtRepository;

	@FXML
	private Button btnGenerate;

	@FXML
	private Button btnClear;

	@FXML
	private Button btnRestore;

	@FXML
	private TextArea txtResult;

	@FXML
	private CheckBox useLombok;

	@FXML
	private CheckBox editPackagesName;

	@FXML
	public void btnClearAction() {
		txtBasePackage.clear();
		txtEntityName.clear();
		txtModel.clear();
		txtService.clear();
		txtController.clear();
		txtRepository.clear();
		txtResult.clear();
	}

	@FXML
	public void btnRestoreAction() {
		txtBasePackage.clear();
		txtEntityName.clear();
		txtResult.clear();
		defaultPackageNames();
	}

	private void defaultPackageNames() {
		txtModel.setText("entities");
		txtService.setText("services");
		txtController.setText("controllers");
		txtRepository.setText("repositories");
	}

	public void editPackagesNameChange(ActionEvent event) {

		if (!editPackagesName.isSelected()) {
			defaultPackageNames();
		}

		txtModel.setDisable(!editPackagesName.isSelected());
		txtService.setDisable(!editPackagesName.isSelected());
		txtController.setDisable(!editPackagesName.isSelected());
		txtRepository.setDisable(!editPackagesName.isSelected());
	}
}
